var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
var fs = require('fs');
var hotels = JSON.parse(fs.readFileSync('hotels.json', 'utf8'));

app.get('/', function (req, res) {
  var queryName = '';
  if (req.query.name) {
    queryName = req.query.name;
  }
  if (queryName === '') {
    res.send(hotels);
  }
  var filtered = hotels.filter(
    hotel => hotel.name.toLowerCase().includes(queryName.toLowerCase())
  );
  res.send(filtered);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
