import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { SideBarService } from './sidebar.service';

@Component({
  selector: 'ngx-sidebar',
  styleUrls: ['./sidebar.component.scss'],
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent {
  constructor(
      private sideBarService: SideBarService
  ) { }

  hotelTitle = '';

  @HostListener('keydown', ['$event'])
  onKeyDown(e) {
    if (e.shiftKey && e.keyCode == 9) {
      console.log('shift and tab');
    }
    if (e.keyCode == 13) {
      this.buscar();
    }
    console.log('pressed', e.keyCode);
  }

  buscar(){
    console.log('jejeje');
    this.sideBarService.buscar(this.hotelTitle);
  }

}
