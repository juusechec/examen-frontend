import { Component, Input, Output, EventEmitter, HostListener, Injectable } from '@angular/core';

@Injectable()
export class SideBarService {

  //https://gist.github.com/mkoczka/2b990523999c82a7c0e4c5db1d1b02a9#file-side-bar-toggle-component-ts
  constructor () {}
  
  hotelTitle = '';

  @Output() change: EventEmitter<string> = new EventEmitter<string>();

  buscar(hotelTitle) {
    this.hotelTitle = hotelTitle;
    this.change.emit(this.hotelTitle);
  }

}
