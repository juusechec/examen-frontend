import { Component, HostBinding } from '@angular/core';
//import { SidebarComponent } from '../../../@theme/components/sidebar/sidebar.component';
import { SideBarService } from '../../../@theme/components/sidebar/sidebar.service';

import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'ngx-form-hotels',
  styleUrls: ['./form-hotels.component.scss'],
  templateUrl: './form-hotels.component.html',
})
export class FormHotelsComponent {

  constructor(
      private sideBarService: SideBarService,
      private http: HttpClient
  ) { }

  @HostBinding('class.hotelTitle')
  hotelTitle = '';

  ngOnInit() {
    this.sideBarService.change.subscribe(hotelTitle => {
      this.hotelTitle = hotelTitle;
      //this.filterHotels();
      this.updateHotels();
    });

    this.updateHotels();
  }

  updateHotels() {
    this.http.get('http://localhost:3000/?name=' + this.hotelTitle)
    .subscribe((data: any[]) => {   // data is already a JSON object
      this.hotels = data;
      this.filteredHotels = data;
      this.selectedHotel = data[0];
    });
  }

  filterHotels() {
    if (this.hotelTitle == '') {
      this.filteredHotels = this.hotels;
      return;
    }
    this.filteredHotels = this.filteredHotels.filter(
      hotel => hotel.name.toLowerCase().includes(this.hotelTitle.toLowerCase())
    );
  }

  hotels: any[] = [];

  filteredHotels: any[] = this.hotels;

  selectedHotel: any = {};
}
