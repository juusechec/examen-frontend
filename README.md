# Propósito

Desarrollar un [examen](EXAMEN.md) de desarrollador frontend.

## Requerimientos
- node >= [8](https://akveo.github.io/nebular/docs/guides/install-based-on-starter-kit#install-tools)
- npm >= [5](https://akveo.github.io/nebular/docs/guides/install-based-on-starter-kit#install-tools)
- n [opcional](https://github.com/tj/n)

## Probado con
- node = v9.4.0
- npm = 5.6.0

## Instalar dependencias Backend
```sh
cd backend
npm install
```

## Iniciar Backend
```sh
cd backend
npm start
```

## Instalar dependencias
```sh
sudo n 9.4.0 # opcional, npm también es alterado
npm install
```

## Iniciar
```sh
npm start --aot
```

## Usar
En otra terminal
```sh
xdg-open http://localhost:9000
```
