# Ejercicio 1: API Rest NodeJS
Este punto consiste en armar un API REST en Node.js. El mismo será utilizado en el ejercicio 2.
La funcionalidad de listado y filtrado de hoteles debe estar soportada por la API y consumida en la aplicación cliente.
A la hora de diseñar la estructura de la aplicación, tener en cuenta factores como escalabilidad, reutilización y separación de responsabilidades.
Uso de configuraciones para ajustar como se ejecuta la aplicación en entornos productivos y de desarrollo.

# Ejercicio 2: Frontend
Maquetar una página de resultado de hoteles, ver imágenes en el repo (solo mobile y desktop).
Consumir la API desarrollada en el ejercicio anterior, implementando las funcionalidades necesarias para listar y filtar los hoteles.
Utilizar alguna librería o framework guiado por componentes ( AngularJS o Angular).
Utilizar algún package manager (npm, bower) para manejar dependencias externas.
Utilizar una grilla responsive o similar para el maquetado.
Optimizar todos los recursos para entornos productivos, (minificar, ofuscar, etc).

# Extras
Los puntos extras solo se tomarán en cuenta si las funcionalidades de los puntos anteriores
fueron completadas correctamente.
Añadir alguna capa de persistencia de datos.
Implementar el CRUD de hoteles (solo a nivel API).

# Entrega
Sube tu solución a un repositorio al que podamos acceder o envianos una archivo comprimido.
Añade un README con las instrucciones para ejecutar la aplicación, así como todo lo que
consideres necesario añadir.
